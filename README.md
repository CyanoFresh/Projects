Projects - solomaha.me
============================

This is catalog with all my projects on solomaha.me domain. Most of them are pet-projects.

DIRECTORY STRUCTURE
-------------------

      css/                  contains styles
      fonts/                contains fonts
      img/                  contains images
      js/                   contains javascripts