<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Projects - Solomaha.me</title>

    <meta name="description" content="Here you can see all my projects on solomaha.me">
    <meta name="keywords" content="Solomaha.me, projects, hub">

    <meta property="og:title" content="Projects - Solomaha.me">
    <meta property="og:description" content="All Solomaha.me projects are stored here. Let's know about it">
    <meta property="og:type" content="website">
    <meta property="og:url" content="http://projects.solomaha.me">
    <meta property="og:image" content="http://projects.solomaha.me/img/icon.png">

    <link rel="stylesheet" href="css/material.min.css">
    <link rel="stylesheet" href="css/site.css">
</head>
<body class="mdl-color--grey-100 mdl-color-text--grey-700 mdl-base">

<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
    <header class="mdl-layout__header">
        <div class="mdl-layout__header-row">
            <!-- Title -->
            <span class="mdl-layout-title">Projects</span>
            <!-- Add spacer, to align navigation to the right -->
            <div class="mdl-layout-spacer"></div>
            <!-- Navigation. We hide it in small screens. -->
            <nav class="mdl-navigation mdl-layout--large-screen-only">
                <a class="mdl-navigation__link" href="/">Home</a>
                <a class="mdl-navigation__link" href="//solomaha.me">Solomaha.me</a>
                <a class="mdl-navigation__link" href="//links.solomaha.me">Links</a>
            </nav>
        </div>
    </header>
    <div class="mdl-layout__drawer">
        <span class="mdl-layout-title">Projects</span>
        <nav class="mdl-navigation">
            <a class="mdl-navigation__link" href="/">Home</a>
            <a class="mdl-navigation__link" href="//solomaha.me">Solomaha.me</a>
            <a class="mdl-navigation__link" href="//links.solomaha.me">Links</a>
        </nav>
    </div>
    <main class="mdl-layout__content">
        <div class="page-content">
            <div class="projects mdl-grid">
                <div class="mdl-card mdl-card--solomaha-me mdl-shadow--2dp mdl-cell mdl-cell--8-col">
                    <div class="mdl-card__title mdl-card--expand">
                        <h2 class="mdl-card__title-text">Solomaha.me</h2>
                    </div>
                    <div class="mdl-card__supporting-text">
                        My personal page with portfolio and contacts. Do you want to know me?
                    </div>
                    <div class="mdl-card__actions mdl-card--border">
                        <a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" href="//solomaha.me/" target="_blank">
                            View project
                        </a>
                    </div>
                </div>
                <div class="mdl-card mdl-card--links mdl-shadow--2dp mdl-cell mdl-cell--4-col">
                    <div class="mdl-card__title mdl-card--expand">
                        <h2 class="mdl-card__title-text">Links</h2>
                    </div>
                    <div class="mdl-card__supporting-text">
                        Make your long url short for free. This is my Pet-project
                    </div>
                    <div class="mdl-card__actions mdl-card--border">
                        <a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" href="//links.solomaha.me/" target="_blank">
                            View project
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>

<script src="js/material.min.js"></script>
<script src="js/site.js"></script>
</body>
</html>